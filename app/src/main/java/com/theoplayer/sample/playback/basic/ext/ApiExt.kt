@file:Suppress("unused") // Context is used to limit function scope

package com.theoplayer.sample.playback.basic.ext

import android.content.Context
import android.content.pm.PackageManager
import android.os.Build

fun Context.isAtLeast(apiLevel: Int): Boolean = Build.VERSION.SDK_INT >= apiLevel
fun Context.isAtLeastLollipop(): Boolean = Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP
fun Context.isAtLeastMarshmallow(): Boolean = Build.VERSION.SDK_INT >= Build.VERSION_CODES.M
fun Context.isAtLeastNougat(): Boolean = Build.VERSION.SDK_INT >= Build.VERSION_CODES.N
fun Context.isAtLeastOreo(): Boolean = Build.VERSION.SDK_INT >= Build.VERSION_CODES.O
fun Context.isAtLeastPie(): Boolean = Build.VERSION.SDK_INT >= Build.VERSION_CODES.P
fun Context.isAtLeastQ(): Boolean = Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q

fun Context.hasFeaturePip(): Boolean = isAtLeastNougat() && packageManager.hasSystemFeature(PackageManager.FEATURE_PICTURE_IN_PICTURE)
