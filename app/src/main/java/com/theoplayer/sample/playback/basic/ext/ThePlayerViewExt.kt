package com.theoplayer.sample.playback.basic.ext

import com.theoplayer.android.api.THEOplayerView

fun THEOplayerView.setUiVisibility(visible: Boolean) {
    // Hide/show the JavaScript control bar UI
    val uiDisplay = if (visible) "" else "none"
    this.evaluateJavaScript("""document.getElementsByClassName("vjs-control-bar")[1].style.display = "$uiDisplay"""") {}
}