package com.theoplayer.sample.playback.basic.ui

import android.R
import android.app.PendingIntent
import android.app.RemoteAction
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.graphics.drawable.Icon
import android.os.Build
import androidx.annotation.RequiresApi

class PipActionReceiver : BroadcastReceiver() {

    override fun onReceive(context: Context?, intent: Intent?) {
        if (context == null || intent == null || intent.action == null) {
            return
        }

        when (intent.action) {
            ACTION_PIP_PLAY -> currentFullscreenSupport?.play()
            ACTION_PIP_PAUSE -> currentFullscreenSupport?.pause()
        }
    }

    companion object {

        var currentFullscreenSupport: FullscreenSupport? = null

        private const val REQUEST_PIP_PLAY = 500
        private const val REQUEST_PIP_PAUSE = 501
        private const val ACTION_PIP_PLAY = "be.vrt.player.lib.PIP_PLAY"
        private const val ACTION_PIP_PAUSE = "be.vrt.player.lib.PIP_PAUSE"

        @RequiresApi(Build.VERSION_CODES.O)
        fun buildPausePlayActionButton(context: Context, isPaused: Boolean): RemoteAction {
            return if (isPaused) {
                buildActionButton(context, R.drawable.ic_media_play, "Hervatten",
                    "Afspelen hervatten", REQUEST_PIP_PLAY, ACTION_PIP_PLAY)
            } else {
                buildActionButton(context, R.drawable.ic_media_pause, "Pauzeren",
                    "Afspelen pauzeren", REQUEST_PIP_PAUSE, ACTION_PIP_PAUSE)
            }
        }

        @RequiresApi(Build.VERSION_CODES.O)
        private fun buildActionButton(
            context: Context, iconResource: Int, actionName: String, iconContentDescription: String, requestCode: Int, action: String
        ): RemoteAction {
            val intent = Intent(context, PipActionReceiver::class.java).setAction(action)
            val pi = PendingIntent.getBroadcast(context, requestCode, intent, PendingIntent.FLAG_CANCEL_CURRENT)
            return RemoteAction(Icon.createWithResource(context, iconResource), actionName, iconContentDescription, pi)
        }

    }
}