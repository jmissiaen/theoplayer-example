package com.theoplayer.sample.playback.basic.model

import com.theoplayer.android.api.THEOplayerConfig

class VideoConfig() {
    internal fun toTheoConfiguration(): THEOplayerConfig {
        return THEOplayerConfig.Builder().build()
    }
}