package com.theoplayer.sample.playback.basic.player

import android.annotation.SuppressLint
import android.app.Activity
import android.util.Log
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.OnLifecycleEvent
import com.theoplayer.android.api.THEOplayerView
import com.theoplayer.android.api.source.SourceDescription
import com.theoplayer.android.api.source.TypedSource
import com.theoplayer.sample.playback.basic.model.VideoConfig
import com.theoplayer.sample.playback.basic.ext.setUiVisibility


@SuppressLint("ViewConstructor") // Disallow inflation from XML, as we wouldn't be able to configure style and features via code
class PlayerView(activity: Activity, private var config: VideoConfig) :
        THEOplayerView(activity, config.toTheoConfiguration()),
        LifecycleObserver {


    init {
        // Assume the context is a LifecycleOwner, typically AppCompatActivity, to handle onResume/onPause/onDestroy
        val lifecycleOwner = context as? LifecycleOwner
        if (lifecycleOwner == null) {
            Log.w(TAG, "Warning: cannot manage VRT video player lifecycle automatically")
        } else {
            lifecycleOwner.lifecycle.addObserver(this)
        }

        // Use the VRT fullscreen activity when going to fullscreen mode
        fullScreenManager.fullscreenActivity = PopoutPlayerActivity::class.java

        // Eagerly set video source
        loadSource()
    }

    @Suppress("MemberVisibilityCanBePrivate") // Part of the public API
    fun loadSource() {
        val typedSource = TypedSource.Builder.typedSource("https://cdn.theoplayer.com/video/elephants-dream/playlist.m3u8")
        val sourceDescription = SourceDescription.Builder
                .sourceDescription(typedSource.build())
                .poster("https://cdn.theoplayer.com/video/elephants-dream/playlist.png")

        player.source = sourceDescription.build()
    }

    /**
     * Manually start playback
     */
    fun play() {
        player.play()
    }

    /**
     * Pause playback
     */
    fun pause() {
        player.pause()
    }

    /**
     * Seek video based on current time in seconds
     */
    fun seekTo(seconds: Double) {
        player.setCurrentTime(seconds)
    }

    /**
     *  Seek video relative from the current time in seconds
     */
    fun seekBy(seconds: Double) {
        player.requestCurrentTime { currentTime ->
            player.setCurrentTime(currentTime + seconds)
        }
    }

    /**
     * Allow TheoPlayer controls ui on the video to show
     */
    fun enablePlayerUi() {
        setUiVisibility(true)
    }

    /**
     * Do not allow TheoPlayer controls ui on the video to show
     */
    fun disablePlayerUi() {
        setUiVisibility(false)
    }

    /**
     * Perform a cleanup when the player is no longer needed
     */
    fun close() {
        onDestroy()
        // Remove observer in case it was auto-added and the activity is not being destroyed
        (context as? LifecycleOwner)?.lifecycle?.removeObserver(this)
    }

    /**
     * Go full-screen ; useful for cases where the player is displayed inline
     */
    fun goFullscreen() {
        fullScreenManager.requestFullScreen()
    }

    fun mute() {
        player.setMuted(true) {}
    }

    fun unmute() {
        player.setMuted(false) {}
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_PAUSE)
    fun lifecyclePause() {
        onPause()
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
    fun lifecycleResume() {
        onResume()
    }

    @Deprecated(
            message = "No longer needed because it is now internally managed",
            replaceWith = ReplaceWith("")
    )
    fun lifecycleDestroy() = Unit

    companion object {
        private const val TAG = "VideoPlayer"
    }

}
