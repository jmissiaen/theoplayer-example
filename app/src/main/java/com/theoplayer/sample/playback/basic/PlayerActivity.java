package com.theoplayer.sample.playback.basic;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.theoplayer.sample.playback.basic.databinding.ActivityPlayerBinding;
import com.theoplayer.sample.playback.basic.model.VideoConfig;
import com.theoplayer.sample.playback.basic.player.FullscreenPlayerActivity;
import com.theoplayer.sample.playback.basic.player.PlayerView;

public class PlayerActivity extends AppCompatActivity {

    private ActivityPlayerBinding viewBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.TheoTheme_Base);
        super.onCreate(savedInstanceState);

        viewBinding = DataBindingUtil.setContentView(this, R.layout.activity_player);

        viewBinding.inlinePlayerPlay.setOnClickListener(v -> {
            playInline();
        });
        viewBinding.inlinePlayerFullScreen.setOnClickListener(v -> {
            playInline();
        });
        viewBinding.fullScreenPlayerPlay.setOnClickListener(v -> {
            playFullScreen();
        });

        // Configuring action bar.
        setSupportActionBar(viewBinding.toolbarLayout.toolbar);
    }

    private void playInline() {
        if (viewBinding.theoPlayerWrapper.getChildCount() > 0) {
            viewBinding.theoPlayerWrapper.removeAllViews();
        }

        PlayerView playerView = new PlayerView(this, new VideoConfig());
        viewBinding.theoPlayerWrapper.addView(playerView);
        playerView.play();

        viewBinding.inlinePlayerFullScreen.setOnClickListener(v -> playerView.goFullscreen());
    }

    private void playFullScreen() {
        final Intent intent = new Intent(this, FullscreenPlayerActivity.class);
        startActivity(intent);
    }
}
