package com.theoplayer.sample.playback.basic.player

import android.content.pm.ActivityInfo
import android.content.res.Configuration
import android.os.Bundle
import com.theoplayer.android.api.fullscreen.FullScreenActivity
import com.theoplayer.sample.playback.basic.ui.FullscreenSupport

// Note: The calling side is responsible for the player cleanup by calling 'playerView.close()'
class PopoutPlayerActivity : FullScreenActivity() {

    private lateinit var fullscreenSupport: FullscreenSupport

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        fullscreenSupport = FullscreenSupport(this, theOplayerView!!)
        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE
    }

    override fun onPause() {
        super.onPause()
        fullscreenSupport.onPause()
    }

    override fun onStop() {
        super.onStop()
        fullscreenSupport.onStop()
    }

    override fun onPictureInPictureModeChanged(
        isInPictureInPictureMode: Boolean,
        newConfig: Configuration?
    ) {
        super.onPictureInPictureModeChanged(isInPictureInPictureMode, newConfig)
        fullscreenSupport.onPictureInPictureModeChanged(isInPictureInPictureMode)
    }

    override fun onWindowFocusChanged(hasFocus: Boolean) {
        super.onWindowFocusChanged(hasFocus)
        fullscreenSupport.onWindowFocusChanged(hasFocus)
    }

    override fun onUserLeaveHint() {
        super.onUserLeaveHint()
        fullscreenSupport.onUserLeaveHint()
    }

    override fun onBackPressed() {
        super.onBackPressed()
        fullscreenSupport.onBackPressed()
    }
}
