package com.theoplayer.sample.playback.basic.player

import android.content.res.Configuration
import android.os.Bundle
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import com.theoplayer.android.api.event.player.PlayerEventTypes
import com.theoplayer.sample.playback.basic.model.VideoConfig
import com.theoplayer.sample.playback.basic.ui.FullscreenSupport

class FullscreenPlayerActivity : AppCompatActivity() {

    private lateinit var playerView: PlayerView
    private lateinit var fullscreenSupport: FullscreenSupport

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        playerView = PlayerView(this, VideoConfig())

        // Add video view
        fullscreenSupport = FullscreenSupport(this, playerView)
        addContentView(playerView, ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
        playerView.player.addEventListener(PlayerEventTypes.ENDED, {
            finish()
        })
    }

    override fun onPause() {
        super.onPause()
        fullscreenSupport.onPause()
    }

    override fun onStop() {
        super.onStop()
        fullscreenSupport.onStop()
    }

    override fun onPictureInPictureModeChanged(isInPictureInPictureMode: Boolean, newConfig: Configuration?) {
        super.onPictureInPictureModeChanged(isInPictureInPictureMode, newConfig)
        fullscreenSupport.onPictureInPictureModeChanged(isInPictureInPictureMode)
    }

    override fun onWindowFocusChanged(hasFocus: Boolean) {
        super.onWindowFocusChanged(hasFocus)
        fullscreenSupport.onWindowFocusChanged(hasFocus)
    }

    override fun onUserLeaveHint() {
        super.onUserLeaveHint()
        fullscreenSupport.onUserLeaveHint()
    }

    override fun onBackPressed() {
        super.onBackPressed()
        fullscreenSupport.onBackPressed()
    }

    override fun onDestroy() {
        playerView.close()
        super.onDestroy()
    }
}
