package com.theoplayer.sample.playback.basic.ui

import android.app.Activity
import android.app.ActivityManager
import android.app.PictureInPictureParams
import android.content.Context
import android.content.Intent
import android.os.Build
import android.view.View
import androidx.annotation.RequiresApi
import com.theoplayer.android.api.THEOplayerView
import com.theoplayer.android.api.event.EventListener
import com.theoplayer.android.api.event.player.PlayerEvent
import com.theoplayer.android.api.event.player.PlayerEventTypes
import com.theoplayer.sample.playback.basic.ext.hasFeaturePip
import com.theoplayer.sample.playback.basic.ext.isAtLeastOreo
import com.theoplayer.sample.playback.basic.ext.setUiVisibility

class FullscreenSupport(
        private val activity: Activity,
        private val playerView: THEOplayerView
) {

    private var hasBeenInPipMode: Boolean = false
    private var hasAddedEventListeners: Boolean = false

    private val playPauseEventListener = EventListener<PlayerEvent<*>> { refreshPipParams() }

    fun onWindowFocusChanged(hasFocus: Boolean) {
        if (hasFocus) {
            hideSystemUi()
        }
    }

    fun onUserLeaveHint() {
        enterPip()
    }

    private fun enterPip() {
        whenSupportsPip {
            PipActionReceiver.currentFullscreenSupport = this
            if (activity.isAtLeastOreo()) {
                activity.enterPictureInPictureMode(pictureInPictureParams())
            } else {
                @Suppress("DEPRECATION") // Still support Android N
                activity.enterPictureInPictureMode()
            }
            hasBeenInPipMode = activity.isInPictureInPictureMode

            if (!hasAddedEventListeners) {
                // Track playing/pausing to update the PiP UI
                playerView.player.addEventListener(PlayerEventTypes.PLAY, playPauseEventListener)
                playerView.player.addEventListener(PlayerEventTypes.PAUSE, playPauseEventListener)
                hasAddedEventListeners = true
            }
        }
    }

    fun onPictureInPictureModeChanged(inPictureInPictureMode: Boolean) {
        playerView.setUiVisibility(!inPictureInPictureMode)
    }

    fun onBackPressed() {
        if (hasBeenInPipMode) {
            // Normally after been in PiP the activity stack gets closed entirely; we want to go back to the app
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                moveLauncherActivityToFront()
            }
        }
    }

    fun onPause() {
        whenSupportsPip {
            if (activity.isInPictureInPictureMode) {
                // Theoplayer pauses itself - we resume playback for smooth continuation
                playerView.player.play()
            }
        }
    }

    fun onStop() {
        PipActionReceiver.currentFullscreenSupport = null
        if (hasAddedEventListeners) {
            playerView.player.removeEventListener(PlayerEventTypes.PLAY, playPauseEventListener)
            playerView.player.removeEventListener(PlayerEventTypes.PAUSE, playPauseEventListener)
            hasAddedEventListeners = false
        }
    }

    fun play() {
        playerView.player.play()
    }

    fun pause() {
        playerView.player.pause()
    }

    private fun refreshPipParams() {
        if (activity.isAtLeastOreo()) {
            activity.setPictureInPictureParams(pictureInPictureParams())
        }
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun pictureInPictureParams(): PictureInPictureParams {
        return PictureInPictureParams.Builder()
                .setActions(listOf(PipActionReceiver.buildPausePlayActionButton(activity, playerView.player.isPaused)))
                .build()
    }

    private fun hideSystemUi() {
        activity.window.decorView.systemUiVisibility = (
                View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                        or View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        or View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        or View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        or View.SYSTEM_UI_FLAG_FULLSCREEN)
    }

    private fun whenSupportsPip(block: () -> Unit) {
        if (activity.hasFeaturePip()) {
            try {
                block()
            } catch (e: IllegalStateException) {
                // Some Samsung devices report PiP support but still crash when its disabled
                // See e.g. https://github.com/android/media-samples/issues/24
            }
        }
    }

    @RequiresApi(Build.VERSION_CODES.M)
    private fun moveLauncherActivityToFront() {
        val activityManager = activity.getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
        val appTasks = activityManager.appTasks
        for (task in appTasks) {
            val baseIntent = task.taskInfo.baseIntent
            val categories = baseIntent.categories
            if (categories != null && categories.contains(Intent.CATEGORY_LAUNCHER)) {
                if (task.taskInfo.numActivities <= 1) {
                    task.moveToFront()
                }
            } else {
                task.finishAndRemoveTask()
            }
        }
    }

}
