How to reproduce the issue:  
1. Click the first button to start inline playback  
2. Click the second button to start a new fullscreen player  
3. Close the new fullscreen player by pressing the device back button  
4. Expand the inline player to fullscreen by pressing the 3rd button or by pressing the full screen button in the player controls  
5. Close the full screen mode by pressing the device back button  
  
Expected => The player continues inline playback  
Actual => The inline player turns black and does no longer respond 